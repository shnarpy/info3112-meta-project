"use strict"

// Jeris Jones March 10
// Projects page component launches as a child from menu component
// Currently unfinished, will eventually use db firestore reference to allow
// CRUD operations on Projects data

// Material components
const {
    colors,
    createMuiTheme,
    MuiThemeProvider,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Paper,
    Grid
} = window['material-ui'];

const theme = createMuiTheme({
    palette: {
        primary: colors.indigo,
        secondary: colors.pink,
        error: colors.red,
        contrastThreshold: 3,
        tonalOffset: 0.2,
    },
    typography: {
        useNextVariants: true,
    },
});

class ProjectsComponent extends React.PureComponent {
    // State will need to keep all projects
    render() {

        return (
            <MuiThemeProvider theme={theme}>
                <Grid container spacing={24}>
                    <Grid item xs={6}>
                        <Typography variant='h6' style={{ textAlign: 'center' }}>Select Project</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant='h6' style={{ textAlign: 'center' }}>User Stories</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Project Name</TableCell>
                                    <TableCell align="right">SomeValue</TableCell>
                                </TableRow>
                            </TableHead>
                        </Table>
                    </Grid>
                    <Grid item xs={6}>
                        <Paper>xs=6</Paper>
                    </Grid>
                </Grid>
            </MuiThemeProvider>
        );
    }
}

const domContainer = document.querySelector("#root");
ReactDOM.render(<ProjectsComponent />, domContainer);