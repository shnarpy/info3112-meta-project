function GetCollections() {
  db.collection("Projects")
    .get()
    .data()
    .then(proj => {
      proj.data;
    });
}

function AddCollection() {
  if (db.collection("Test").get() != null) {
    db.AddCollection("Test");
  }
}

function AddDocument(projectName) {
  var docToAdd = db
    .collection("Projects")
    .where(projectName, "==", "" /*textbox.Text field?*/)
    .get();

  db.AddDocument(docToAdd);
}

function UpdateProjectsDocument(doc) {
  doc.UpdateDocument();
}

function UpdateUserStoryDocument(doc, pts, desc) {
  doc.UpdateDocument({
    points: pts,
    description: desc
  });
}

function DeleteBacklogCollection(backlogColl) {
  let id = db.db.remove(backlogColl);
}

function DeleteProjectCollection(projColl) {
  db(`projects/${projColl}`).remove();
}

function DeleteDocument(doc) {
  db.remove(doc);
}

function DeleteUserStoryDocument(doc) {
  db.remove(doc);
}
