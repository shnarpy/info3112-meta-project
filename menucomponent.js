"use strict";

// Material Components
const {
  AppBar,
  colors,
  createMuiTheme,
  Icon,
  IconButton,
  Menu,
  MenuItem,
  MuiThemeProvider,
  Toolbar,
  Typography,
  withStyles
} = window["material-ui"];

const theme = createMuiTheme({
  palette: {
    primary: {
      light: colors.purple[300],
      main: colors.purple[500],
      dark: colors.purple[700]
    },
    secondary: {
      light: colors.green[300],
      main: colors.green[500],
      dark: colors.green[700]
    }
  },
  typography: {
    useNextVariants: true
  }
});
const styles = theme => ({
  root: {
    textAlign: "center",
    paddingTop: theme.spacing.unit * 20
  },
  icon: {
    marginRight: theme.spacing.unit
  }
});

class MenuComponent extends React.PureComponent {
  state = {
    anchorEl: null
  };
  onMenuItemClicked = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  onClose = () => {
    this.setState({ anchorEl: null });
  };
  render() {
    const { anchorEl } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar>
            <IconButton onClick={this.onMenuItemClicked} color="inherit" />
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.onClose}
            >
              <MenuItem>Home</MenuItem>
              <MenuItem>Load Users</MenuItem>
            </Menu>
            <Typography variant="h6" color="inherit">
              HJK Project Tracker
            </Typography>
          </Toolbar>
        </AppBar>
      </MuiThemeProvider>
    );
  }
}
const menuContainer = document.querySelector("#menu");
ReactDOM.render(<MenuComponent />, menuContainer);
