"use strict";

// Jeris Jones March 10
// Home page component launched as child from menu component

// Material components
const {
    colors,
    createMuiTheme,
    MuiThemeProvider,
    Typography
} = window['material-ui'];

const theme = createMuiTheme({
    palette: {
        primary: colors.indigo,
        secondary: colors.pink,
        error: colors.red,
        contrastThreshold: 3,
        tonalOffset: 0.2,
    },
    typography: {
        useNextVariants: true,
    },
});

class HomeComponent extends React.PureComponent {

    render() {

        const logoStyle = {
            display: 'block',
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: '2%',
            height: '100px',
            width: '100px'
        }

        return (
            <MuiThemeProvider theme={theme}>
                <Typography style={{ textAlign: 'center' }} variant="h5">
                    HJK Project Tracker
                </Typography>
                <img src='https://i.imgur.com/3VqrnNS.png' alt='logo' style={logoStyle} />
            </MuiThemeProvider>
        );
    }
}

const domContainer = document.querySelector("#root");
ReactDOM.render(<HomeComponent />, domContainer);
