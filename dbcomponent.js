"use strict";

const e = React.createElement;

class AddDocButton extends React.PureComponent {
  state = { added: false };

  onAddClick = () => {
    this.setState({ added: true });
    // 'db' refers to the const declared in html file script
    db.collection("projects").add({
      name: "projAddedFromWeb"
    });
  };

  render() {
    if (this.state.added) {
      return "Document added";
    }

    return e("button", { onClick: () => this.onAddClick() }, "Add Doc");
  }
}

const domContainer = document.querySelector("#add_button_container");
ReactDOM.render(e(AddDocButton), domContainer);
